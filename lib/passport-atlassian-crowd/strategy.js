/**
 * Module dependencies.
 */
var passport = require('passport'),
  https = require('https'),
  http = require('http'),
  URL = require('url'),
  util = require('util'),
  BadRequestError = require('./errors/badrequesterror');


/**
 * `Strategy` constructor.
 *
 * The local authentication strategy authenticates requests based on the
 * credentials submitted through an HTML-based login form.
 *
 * Applications must supply a `verify` callback which accepts `username` and
 * `password` credentials, and then calls the `done` callback supplying a
 * `user`, which should be set to `false` if the credentials are not valid.
 * If an exception occured, `err` should be set.
 *
 * Optionally, `options` can be used to change the fields in which the
 * credentials are found.
 *
 * Options:
 *   - `usernameField`  field name where the username is found, defaults to _username_
 *   - `passwordField`  field name where the password is found, defaults to _password_
 *   - `passReqToCallback`  when `true`, `req` is the first argument to the verify callback (default: `false`)
 *
 * Examples:
 *
 *     passport.use(new LocalStrategy(
 *       function(username, password, done) {
 *         User.findOne({ username: username, password: password }, function (err, user) {
 *           done(err, user);
 *         });
 *       }
 *     ));
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options, verify) {
    if (typeof options == 'function') {
        verify = options;
        options = {};
    }
    if (!verify) throw new Error('atlassian-crowd authentication strategy requires a verify function');

    if (!options.crowdServer) {
        throw new Error("atlassian-crowd strategy requires a crowd server url");
    }

    this._crowdServer = options.crowdServer;
    this._crowdApplication = options.crowdApplication;
    this._crowdApplicationPassword = options.crowdApplicationPassword;

    this._usernameField = options.usernameField || 'username';
    this._passwordField = options.passwordField || 'password';
    this._tokenField = options._tokenField || 'token';

    passport.Strategy.call(this);
    this.name = 'atlassian-crowd';
    this._verify = verify;
    this._retrieveGroupMemberships = options.retrieveGroupMemberships;
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);


function _parseSessionResponse(res) {
    return {
        value: res.token,
        expiryDate: res['expiry-date'],
        createdDate: res['created-date']
    };
};

function _parseUser(user) {
    var parsedUser = {
        provider: 'atlassian-crowd',
        id: user.name,
        username: user.name,

        displayName: user['display-name'],
        name: {
            firstName: user['first-name'],
            lastName: user['last-name']
        },
        email: user.email,
        //emails: [
        //    { value: user.email }
        //],
        //_json: user

        attributes: {}
    };

    // loop through attributes
    user.attributes.attributes.forEach(function(attr) {
        parsedUser.attributes[attr.name] = attr.values;
    });

    return parsedUser;
};

function _parseProfile(crowdUser) {
    return {
        provider:'atlassian-crowd',
        id:crowdUser.name,
        username:crowdUser.name,
        token: crowdUser.token,
        displayName:crowdUser["display-name"],
        name:{
            familyName:crowdUser["last-name"],
            givenName:crowdUser["first-name"]
        },
        email:crowdUser.email,
        emails:[
            {value:crowdUser.email}
        ],
        _json:crowdUser
    };
}

function _lookup(obj, field) {
    if (!obj) {
        return null;
    }
    var chain = field.split(']').join('').split('['); // TODO(fotomut): not sure what this does, maybe its to strip '[]' from a string?
    for (var i = 0, len = chain.length; i < len; i++) {
        var prop = obj[chain[i]];
        if (typeof(prop) === 'undefined') {
            return null;
        }
        if (typeof(prop) !== 'object') {
            return prop;
        }
        obj = prop;
    }
    return null;
}


function _handleResponse(response, callback) {
    var result = "";
    response.on("data", function (chunk) {
        result += chunk;
    });
    response.on("close", function (err) {
        callback(response, result);
    });
    response.addListener("end", function () {
        callback(response, result);
    });
}

/**
 * Authenticate request based on the contents of a form submission.
 *
 * @param {Object} req
 * @api protected
 */
Strategy.prototype.authenticate = function (req, options) {
    options = options || {};
    var username = _lookup(req.params, this._usernameField) || _lookup(req.body, this._usernameField) || _lookup(req.query, this._usernameField);
    var password = _lookup(req.params, this._passwordField) || _lookup(req.body, this._passwordField) || _lookup(req.query, this._passwordField);
    var token = _lookup(req.params, this._tokenField) || _lookup(req.body, this._tokenField) || _lookup(req.query, this._tokenField);

    if (!((username && password) || token)) {
        return this.fail(new BadRequestError(options.badRequestMessage || 'Missing credentials'));
    }
    var self = this;

    var http_library = https;
    var parsedUrl = URL.parse(this._crowdServer, true);
    if (parsedUrl.protocol == "https:" && !parsedUrl.port) {
        parsedUrl.port = 443;
    }

    // As this is OAUth2, we *assume* https unless told explicitly otherwise.
    if (parsedUrl.protocol != "https:") {
        http_library = http;
    }

    var postData = {};
    var sessionPath = parsedUrl.pathname + 'rest/usermanagement/1';
    if (token) {
        sessionPath += '/session/' + token;
        postData = JSON.stringify({
            validationFactors: [{
                name: 'remote_address',
                value: req.connection.remoteAddress
            }]
        });
    } else { // no token, use username/password
        sessionPath += '/session';
        postData = JSON.stringify({
            username: username,
            password: password,
            'validation-factors': {
                validationFactors: [{
                    name: 'remote_address',
                    value: req.connection.remoteAddress
                }]
            }
        });
    }
    var applicationAuth = 'Basic ' + new Buffer(this._crowdApplication + ':' + this._crowdApplicationPassword).toString('base64');

    function verified(err, user, info) {
        if (err) {
            return self.error(err);
        }
        if (!user) {
            return self.fail(info);
        }
        self.success(user, info);
    }

    function handleGroupResponse(response, result) {
        if (response.statusCode === 200) {
            var resultObject = JSON.parse(result);
            var groups = [];
            resultObject.groups.forEach(function (group) {
                // JIRA uses an older version of the Crowd REST API
                if (group.GroupEntity) {
                    groups.push(group.GroupEntity.name);
                }
                else {
                    groups.push(group.name);
                }
            });

            return groups;

        } else if (response.statusCode >= 400 && response.statusCode < 500) {
            var error = JSON.parse(result);
            // TODO: add more error info
            console.log("passport-atlassian-crowd: Error retrieving groups for user '" + username + "': " + error.message + " [" + error.reason + "]");
            return self.fail(error);
        } else {
            return self.error(new Error("Invalid response from Crowd Server '" + self._crowdServer +
            "' [" + response.statusCode + "]: " + result));
        }
    }

    function handleAuthenticationResponse(response, result) {
        if (response.statusCode === 200) {
            var crowdUser = JSON.parse(result);
            var userprofile = _parseProfile(crowdUser);
            userprofile._raw = result;

            if (self._retrieveGroupMemberships) {
                var groupResult = "";
                var groupRequest = http_library.get({
                    host:parsedUrl.hostname,
                    port:parsedUrl.port,
                    path:parsedUrl.pathname + "rest/usermanagement/1/user/group/nested?username=" + username,
                    headers:{
                        "Content-Type":"application/json",
                        "Accept":"application/json",
                        "Authorization":applicationAuth
                    }
                }, function (response) {
                    _handleResponse(response, function (response, groupResult) {
                        userprofile.groups = handleGroupResponse(response, groupResult);
                        return self._verify(userprofile, verified);
                    });
                });
                groupRequest.on('error', function (err) {
                    self.error(new Error("Error connecting to Crowd Server '" + self._crowdServer + "': " + err));
                });
            } else {
                return self._verify(userprofile, verified);
            }
        } else if (response.statusCode >= 400 && response.statusCode < 500) {
            var error = {"message":result};
            try {
                error = JSON.parse(result);
            } catch (err) {
            }

            var logMsg = "Error authenticating user '" + username + "' on '" + self._crowdServer + "': " + error.message;
            if (error.reason) {
                logMsg += " [" + error.reason + "]";
            }
            console.log('passport-atlassian-crowd: ' + logMsg);
            return self.fail(error);
        } else {
            return self.error(new Error("Invalid response from Crowd Server '" + self._crowdServer +
            "' [" + response.statusCode + "]: " + result));
        }
    }

    function handleSessionResponse(response, sessionResult) {
        if (response.statusCode === 201
          || response.statusCode === 200) {
            var tokenInfo = _parseSessionResponse(JSON.parse(sessionResult));
            //tokenInfo._raw = sessionResult;
            if (username && JSON.parse(sessionResult).user.name !== username) {
                console.error('passport-atlassian-crowd: username didnt match with result from Crowd session endpoint!  username='+username+ '; sessionResult.user.name=' + JSON.parse(sessionResult).user.name);
                // TODO: figure out this error case and what to do with it.
                return self.error(new Error('username didnt match with result from Crowd session endpoint!  username='+username+ '; sessionResult.user.name=' + JSON.parse(sessionResult).user.name));
            } else {
                username = JSON.parse(sessionResult).user.name;
            }

            var userResult = '';
            var userRequest = http_library.get({
                host: parsedUrl.hostname,
                port: parsedUrl.port,
                path: parsedUrl.pathname + 'rest/usermanagement/1/user?username=' + username + '&expand=attributes',
                headers:{
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": applicationAuth
                }
            }, function(response) {
                _handleResponse(response,  function(response, userResult) {
                    var userProfile = _parseUser(JSON.parse(userResult));
                    userProfile.token = tokenInfo;
                    //userProfile._raw = userResult;

                    if (self._retrieveGroupMemberships) {
                        var groupResult = '';
                        var groupRequest = http_library.get({
                            host: parsedUrl.hostname,
                            port: parsedUrl.port,
                            path: parsedUrl.pathname + 'rest/usermanagement/1/user/group/nested?username=' + username,
                            headers:{
                                "Content-Type": "application/json",
                                "Accept": "application/json",
                                "Authorization": applicationAuth
                            }
                        }, function (response) {
                            _handleResponse(response, function (response, groupResult) {
                                userProfile.groups = handleGroupResponse(response, groupResult);
                                return self._verify(userProfile, verified);
                            });
                        });
                        groupRequest.on('error', function (err) {
                            self.error(new Error('Error connecting to Crowd Server \'' + self._crowdServer + '\': ' + err));
                        });
                    } else {
                        return self._verify(userProfile, verified);
                    }
                });
            });


        } else if (response.statusCode >= 400 && response.statusCode < 500) {
            var error = { message: sessionResult };
            try {
                error = JSON.parse(sessionResult);
            } catch (err) {
            }

            var logMsg = 'Error authenticating user \'' + username + '\' on \'' + self._crowdServer + '\' (statusCode=' + response.statusCode +  ': ' + error.message;
            if (error.reason) {
                logMsg += " [" + error.reason + "]";
            }
            console.log('passport-atlassian-crowd: ' + logMsg);
            return self.fail(error);
        } else {
            return self.error(new Error("Invalid response from Crowd Server '" + self._crowdServer +
            "' [" + response.statusCode + "]: " + sessionResult));
        }
    }

    var crowdRequest = http_library.request({
        host:parsedUrl.hostname,
        port:parsedUrl.port,
        path:sessionPath,
        method:"POST",
        headers:{
            "Content-Type":"application/json",
            "Accept":"application/json",
            "Content-Length":postData.length,
            "Authorization":applicationAuth
        }
    }, function (response) {
        _handleResponse(response, handleSessionResponse);
    });
    crowdRequest.on('error', function (err) {
        self.error(new Error("Error connecting to Crowd Server '" + self._crowdServer + "': " + err));
    });

    crowdRequest.write(postData);
    crowdRequest.end();
};

/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
